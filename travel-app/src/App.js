import React, { Component } from 'react';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
import './App.css';
import TopHeader from './components/layout/top-header/top-header'
import CenterHeader from './components/layout/center-header/center-header'
import SlideHeader from './components/layout/slide-header/slide-header'
import SearchHeader from './components/layout/search-header/search-header'
import OptionHeader from './components/layout/option-header/option-header'
import TopFooter from './components/layout/top-footer/top-footer'
import BotFooter from './components/layout/bot-footer/bot-footer'
import Home from './components/content/home/home'
import DanhSachTour from './components/content/danh-sach-tour/danh-sach-tour';
import ChiTietTour from './components/content/chi-tiet-tour/chi-tiet-tour';

export default class App extends Component {
    render() {
        return (
            <div>
                <TopHeader></TopHeader>
                <div className="wrapper-header">
                    <CenterHeader></CenterHeader>
                </div>
                <SlideHeader></SlideHeader>
                <SearchHeader></SearchHeader>
                <OptionHeader></OptionHeader>
                <div>
                    <Router>
                        <div>
                            <Route exact  path="/" component={Home} />
                            <Route path="/trong-nuoc" component={DanhSachTour} />
                            <Route path="/chi-tiet-tour" component={ChiTietTour} />
                        </div>
                    </Router>
                </div>
                <div className="footer">
                    <TopFooter></TopFooter>
                    <BotFooter></BotFooter>
                </div>
            </div>
        );
    }
}