import React, { Component } from 'react';
import './lich-trinh.css';
import { Timeline, TimelineItem } from 'vertical-timeline-component-for-react';


class LichTrinh extends Component {
    render() {
        return (
            <div>
                <Timeline lineColor={'#ddd'}>
                    <TimelineItem
                        key="001"
                        dateText="0.1 09/08/2019"
                        style={{ color: '#e86971' }}
                    >
                        <div>
                            <h5 className="text-title"><i className="fa fa-map-marker" aria-hidden="true"></i> TP.HCM - PHÚ QUỐC - SUNSET SANATO BEACH CLUB Số bữa ăn: 1 bữa (trường hợp chuyến bay trước 11h, Vietravel sẽ hỗ trợ Quý khách bữa ăn trưa)</h5>
                        </div>
                        <p className="font-style">
                            Quý khách tập trung tại Sân bay Tân Sơn Nhất, ga đi Trong Nước. Hướng dẫn viên làm thủ tục cho Quý khách đáp chuyến bay đi Phú Quốc. Xe đón đoàn tại sân bay đưa Quý khách khởi hành viếng Chùa Sư Muôn (Hùng Long Tự) - để cầu nguyện sự an lành và hạnh phúc đến với gia đình. Tiếp tục, đoàn tham quan Làng Chài Hàm Ninh, Vườn Tiêu, Nhà Thùng làm nước mắm, Cơ Sở nuôi cấy Ngọc Trai. Quý khách ghé thăm Dinh Cậu - biểu tượng văn hoá và tín ngưỡng của đảo Phú Quốc, là nơi cầu may mắn, cầu an lành và là nơi ngư dân địa phương gởi gắm niềm tin cho một chuyến ra khơi đánh bắt đầy ắp cá khi trở về. Sau đó nhận phòng khách sạn nghỉ ngơi.
                        </p>
                        <p className="font-style">
                            Buổi chiều xe đưa quý khách tham quan Sunset Sanato Beach Club sở hữu vị thế đắc địa nhất, khu vui chơi nằm ngay trung tâm của Bãi Trường. Nơi đây, sóng biển dịu êm như một dòng sông, bạn sẽ thấy mình được hưởng trọn vẹn hương vị mặn mòi của biển và mát lành của những hoang sơ còn vương vấn, đến đây du khách sẽ được thưởng ngoạn phong cảnh biển tuyệt đẹp, những góc ảnh check-in cực chất, tự do tắm biển, hòa nhịp trong những bản nhạc sôi động . Nghỉ đêm tại Phú Quốc.
                        </p>
                    </TimelineItem>
                    <TimelineItem
                        key="002"
                        dateText="0.2 09/08/2019"
                        style={{ color: '#e86971' }}
                    >
                        <div>
                            <h5 className="text-title"><i className="fa fa-map-marker" aria-hidden="true"></i> THIỀN VIỆN HỘ QUỐC - BÃI SAO Số bữa ăn: 2 bữa (sáng, trưa)</h5>
                        </div>
                        <p className="font-style">
                            Xe đưa Quý khách đi tham quan Nam Đảo, Quý khách đến viếng Thiền Viện Trúc Lâm Hộ Quốc - ngôi chùa đẹp và lớn nhất đảo ngọc, với nhiều công trình như: Đại hùng bảo điện, lầu chuông, lầu trống, nhà Tổ với vật liệu bằng gỗ lim đưa về từ Nam Phi và đá nguyên thủy có giá trị sử dụng từ 700 - 1.000 năm… Với khung cảnh hoang sơ, yên tĩnh, tạo nên một khung cảnh thiên nhiên đặc sắc đầy vẻ tôn nghiêm và thanh tịnh. Tiếp đến đoàn vào Bãi Sao - một bãi biển dịu êm, bãi cát dài tĩnh lặng và nguyên sơ nơi đảo xanh. Tại đây Quý khách sẽ thật sự cảm thấy yên bình, thư thái và dường như cuộc sống chậm lại khi hòa mình cùng thiên nhiên. Ăn trưa, về lại khách sạn tự do nghỉ ngơi.
                        </p>
                        <p className="font-style">
                            Buổi chiều, Quý khách tự do tham quan Khu vui chơi giải trí Vinpearl Land (chi phí tự túc) rộng lớn với đầy đủ những trò chơi dành cho mọi lứa tuổi và sở thích. Nơi đây hội đủ tất cả các trò chơi trong nhà, ngoài trời như: đĩa bay, đu quay con ong, cốc xoay, đĩa quay siêu tốc, cối xay gió,… Khu công viên nước với các trò chơi hấp dẫn như: boomerang khổng lồ, đường trượt siêu lòng chảo, dòng sông lười, lâu đài, bể tạo sóng… Tiếp tục Quý khách có thể khám phá Khu Thủy Cung với hàng trăm loài cá và sinh vật biển kỳ thú như: cá Hải tượng, cá Sấu hỏa tiễn với chiếc mõm dài và hung dữ hay những loài quý hiếm như cá Nemo, cá Napoleon, cá Mập trắng, cua King Crab…. Hơn nữa, ở đây  còn có Rạp chiếu phim 5D, Sân khấu nhạc nước. Hay trải nghiệm tại Vinpearl Safari - Vườn Thú Hoang Dã đầu tiên tại Việt Nam (chi phí tự túc) với quy mô 180ha, hơn 130 loài động vật quý hiếm và các chương trình Biểu diễn động vật, Chụp ảnh với động vật, Khám phá và trải nghiệm Vườn thú mở trong rừng tự nhiên, gần gũi và thân thiện với con người. Quý khách tự túc ăn tối. Nghỉ đêm tại khách sạn.
                        </p>
                    </TimelineItem>
                    <TimelineItem
                        key="003"
                        dateText="0.3 09/08/2019"
                        style={{ color: '#e86971' }}
                    >
                        <div>
                            <h5 className="text-title"><i className="fa fa-map-marker" aria-hidden="true"></i> PHÚ QUỐC - TP.HCM Số bữa ăn: 1 bữa (trường hợp chuyến bay sau 14h, Vietravel sẽ hỗ trợ Quý Khách bữa ăn trưa)</h5>
                        </div>
                        <p className="font-style">
                            Sau khi ăn sáng. Tự do tắm biển nghỉ ngơi. Đến giờ trả phòng khách sạn. Sau đó, xe đưa Quý khách ra sân bay Phú Quốc đáp chuyến bay trở về Tp.HCM. Chia tay Quý khách và kết thúc chương trình du lịch tại sân bay Tân Sơn Nhất.
                        </p>
                    </TimelineItem>
                </Timeline>
            </div>
        );
    }
}

export default LichTrinh;