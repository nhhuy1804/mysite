import React, { Component } from 'react';
import './ua-thich.css';

class UaThich extends Component {
    render() {
        return (
            <div>
                <div>
                    <h3>ĐIỂM ĐẾN ƯU THÍCH</h3>
                </div>
                <div className="flex-ua-thich">
                    <div className="box-ua-thich no-margin-left">
                        <div className="img-hover-zoom-ut">
                            <img src="/images/home/uathich/dd1.jpg" alt="" className="img-ua-thich" />
                        </div>
                        <div className="context-text">
                            <span className="hover-red">VỊNH HẠ LONG</span>
                            <p className="no-margin-bot">------------------</p>
                            <span>Đã có <a className="hover-red">+1,900</a> lượt khách</span>
                        </div>
                    </div>
                    <div className="box-ua-thich">
                        <div className="img-hover-zoom-ut">
                            <img src="/images/home/uathich/dd2.jpg" alt="" className="img-ua-thich" />
                        </div>
                        <div className="context-text">
                            <span className="hover-red">SAPA</span>
                            <p className="no-margin-bot">------------------</p>
                            <span>Đã có <a className="hover-red">+1,600</a> lượt khách</span>
                        </div>
                    </div>
                    <div className="box-ua-thich">
                        <div className="img-hover-zoom-ut">
                            <img src="/images/home/uathich/dd3.jpg" alt="" className="img-ua-thich" />
                        </div>
                        <div className="context-text">
                            <span className="hover-red">ĐÀ NẴNG</span>
                            <p className="no-margin-bot">------------------</p>
                            <span>Đã có <a className="hover-red">+1,700</a> lượt khách</span>
                        </div>
                    </div>
                    <div className="box-ua-thich no-margin-left">
                        <div className="img-hover-zoom-ut">
                            <img src="/images/home/uathich/dd4.jpg" alt="" className="img-ua-thich" />
                        </div>
                        <div className="context-text">
                            <span className="hover-red">QUI NHƠN</span>
                            <p className="no-margin-bot">------------------</p>
                            <span>Đã có <a className="hover-red">+1,200</a> lượt khách</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default UaThich;