import React, { Component } from 'react';
import './uu-dai.css';

class UuDai extends Component {
    render() {
        return (
            <div>
                <div>
                    <h3>ƯU ĐÃI ĐÃ THANH TOÁN TRỰC TUYẾN</h3>
                </div>
                <div className="flex-uu-dai">
                    <div className="box-uu-dai">
                        <div className="img-hover-zoom">
                            <img src="/images/home/uudai/uudai1.jpg" alt="" className="img-uu-dai" />
                        </div>
                        <div className="content-box">
                            <label className="title-uu-dai mauchudao">Đà Lạt - Madagui - Đồi chè Cầu Đất - Cà phê Mê Linh (Khách sạn 4*. Tour Tiết Kiệm)</label>
                        </div>
                        <div className="content-uu-dai">
                            <span className="float-l">18/07/2019 07:00 | 4 ngày</span>
                            <span className="float-r">1 chỗ</span>
                        </div>
                        <div className="price-box float-l">
                            <span className="through-line">5.000.000đ</span>
                            <span className="price-hight-light">&nbsp;&nbsp;4.000.000đ</span>
                        </div>
                    </div>
                    <div className="box-uu-dai">
                        <div className="img-hover-zoom">
                            <img src="/images/home/uudai/uudai2.jpg" alt="" className="img-uu-dai" />
                        </div>
                        <div className="content-box">
                            <label className="title-uu-dai mauchudao">Vân Đồn - Hạ Long - Bái Đính - Tràng An - Tuyệt Tịnh Cốc - Tam Chúc - Hà Nội (Tour Tiết Kiệm)</label>
                        </div>
                        <div className="content-uu-dai">
                            <span className="float-l">18/07/2019 07:00 | 4 ngày</span>
                            <span className="float-r">2 chỗ</span>
                        </div>
                        <div className="price-box float-l">
                            <span className="through-line">7.900.000đ</span>
                            <span className="price-hight-light">&nbsp;&nbsp;6.600.000đ</span>
                        </div>
                    </div>
                    <div className="box-uu-dai">
                        <div className="img-hover-zoom">
                            <img src="/images/home/uudai/uudai3.jpg" alt="" className="img-uu-dai" />
                        </div>
                        <div className="content-box">
                            <label className="title-uu-dai mauchudao">Singapore (Thủy cung S.E.A Aquarium & Madame Tussauds, Tour Tiêu Chuẩn)</label>
                        </div>
                        <div className="content-uu-dai">
                            <span className="float-l">18/07/2019 08:00 | 3 ngày</span>
                            <span className="float-r">1 chỗ</span>
                        </div>
                        <div className="price-box float-l">
                            <span className="through-line">15.300.000đ</span>
                            <span className="price-hight-light">&nbsp;&nbsp;12.900.000đ</span>
                        </div>
                    </div>
                    <div className="box-uu-dai">
                        <div className="img-hover-zoom">
                            <img src="/images/home/uudai/uudai4.jpg" alt="" className="img-uu-dai" />
                        </div>
                        <div className="content-box">
                            <label className="title-uu-dai mauchudao">Hà Nội - Lào Cai - Sapa - Chinh phục nóc nhà Đông Dương Fansipan - Yên Tử - Hạ Long - Bái Đính - Tràng An - Tặng Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)</label>
                        </div>
                        <div className="content-uu-dai">
                            <span className="float-l">18/07/2019 09:00 | 6 ngày</span>
                            <span className="float-r">5 chỗ</span>
                        </div>
                        <div className="price-box float-l">
                            <span className="through-line">9.500.000đ</span>
                            <span className="price-hight-light">&nbsp;&nbsp;7.700.000đ</span>
                        </div>
                    </div>
                </div>
                <div className="margin-t">
                    <button className="form-control xem-them-btn">Xem tất cả</button>
                </div>
            </div>
        );
    }
}

export default UuDai;