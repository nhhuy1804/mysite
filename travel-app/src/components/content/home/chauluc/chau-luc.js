import React, { Component } from 'react';
import './chau-luc.css';

class ChauLuc extends Component {
    render() {
        return (
            <div>
                <div className="flex-chau-luc">
                    <div className="box-chau-luc">
                        <div className="img-hover-zoom-cl">
                            <img src="/images/home/chauluc/qg1.jpg" alt="" className="img-chau-luc" />
                        </div>
                        <div className="context-text-cl">
                            <span className="hover-red">CHÂU ÂU</span><br />
                            <a className="mauchudao">Khám phá ngay <i className="fa fa-caret-right"></i></a>
                        </div>
                    </div>
                    <div className="box-chau-luc">
                        <div className="img-hover-zoom-cl">
                            <img src="/images/home/chauluc/qg2.jpg" alt="" className="img-chau-luc" />
                        </div>
                        <div className="context-text-cl">
                            <span className="hover-red">CHÂU Á</span><br />
                            <a className="mauchudao">Khám phá ngay <i className="fa fa-caret-right"></i></a>
                        </div>
                    </div>
                    <div className="box-chau-luc">
                        <div className="img-hover-zoom-cl">
                            <img src="/images/home/chauluc/qg3.jpg" alt="" className="img-chau-luc" />
                        </div>
                        <div className="context-text-cl">
                            <span className="hover-red">CHÂU ÚC</span><br />
                            <a className="mauchudao">Khám phá ngay <i className="fa fa-caret-right"></i></a>
                        </div>
                    </div>
                    <div className="box-chau-luc">
                        <div className="img-hover-zoom-cl">
                            <img src="/images/home/chauluc/qg4.jpg" alt="" className="img-chau-luc" />
                        </div>
                        <div className="context-text-cl">
                            <span className="hover-red">CHÂU MỸ</span><br />
                            <a className="mauchudao">Khám phá ngay <i className="fa fa-caret-right"></i></a>
                        </div>
                    </div>
                    <div className="box-chau-luc">
                        <div className="img-hover-zoom-cl">
                            <img src="/images/home/chauluc/qg5.jpg" alt="" className="img-chau-luc" />
                        </div>
                        <div className="context-text-cl">
                            <span className="hover-red">CHÂU PHI</span><br />
                            <a className="mauchudao">Khám phá ngay <i className="fa fa-caret-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ChauLuc;