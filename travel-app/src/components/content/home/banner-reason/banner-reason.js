import React, { Component } from 'react';
import './banner-reason.css';

class BannerReason extends Component {
    render() {
        return (
            <div className="banner">
                <div className="row">
                    <div className="col-12 col-sm-6 col-md-3 col-lg-3 margin-bot">
                        <span className="fa-stack mauchudao">
                            <span className="fa fa-circle-o fa-stack-2x"></span>
                            <strong className="fa-stack-1x">
                                1
                            </strong>
                        </span>
                        <div className="why-area">
                            <p className="p-title mauchudao">MẠNG BÁN TOUR</p>
                            <p className="p-des">Số 1 tại Việt Nam</p>
                            <p className="p-des">Ứng dụng công nghệ mới nhất</p>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-3 col-lg-3 margin-bot">
                        <span className="fa-stack mauchudao">
                            <span className="fa fa-circle-o fa-stack-2x"></span>
                            <strong className="fa-stack-1x">
                                2
                            </strong>
                        </span>
                        <div className="why-area">
                            <p className="p-title mauchudao">THANH TOÁN</p>
                            <p className="p-des">An toàn và linh hoạt</p>
                            <p className="p-des">Liên kết với các tổ chức tài chính</p>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-3 col-lg-3 margin-bot">
                        <span className="fa-stack mauchudao">
                            <span className="fa fa-circle-o fa-stack-2x"></span>
                            <strong className="fa-stack-1x">
                                3
                            </strong>
                        </span>
                        <div className="why-area">
                            <p className="p-title mauchudao">GIÁ CẢ</p>
                            <p className="p-des">Luôn có mức giá tốt nhất</p>
                            <p className="p-des">Đảm bảo giá tốt</p>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-3 col-lg-3 margin-bot">
                        <span className="fa-stack mauchudao">
                            <span className="fa fa-circle-o fa-stack-2x"></span>
                            <strong className="fa-stack-1x">
                                4
                            </strong>
                        </span>
                        <div className="why-area">
                            <p className="p-title mauchudao">SẢN PHẨM</p>
                            <p className="p-des">Đa dạng, chất lượng</p>
                            <p className="p-des">Đạt chất lượng tốt nhất</p>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-3 col-lg-3 margin-bot">
                        <span className="fa-stack mauchudao">
                            <span className="fa fa-circle-o fa-stack-2x"></span>
                            <strong className="fa-stack-1x">
                                5
                            </strong>
                        </span>
                        <div className="why-area">
                            <p className="p-title mauchudao">ĐẶT TOUR</p>
                            <p className="p-des">Dễ dàng và nhanh chóng</p>
                            <p className="p-des">Đặt tour chỉ với 3 bước</p>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-3 col-lg-3 margin-bot">
                        <span className="fa-stack mauchudao">
                            <span className="fa fa-circle-o fa-stack-2x"></span>
                            <strong className="fa-stack-1x">
                                6
                            </strong>
                        </span>
                        <div className="why-area">
                            <p className="p-title mauchudao">HỖ TRỢ</p>
                            <p className="p-des">20 / 7</p>
                            <p className="p-des">Hotline và hỗ trợ trực tuyến</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default BannerReason;