import React, { Component } from 'react';
import './tour-gio-chot.css';
import Countdown from 'react-countdown-now';

class TourGioChot extends Component {
    render() {
        return (
            <div>
                <div className='row'>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                        <img src="/images/home/tgc2.jpg" alt="" className="img-tgc" />
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 flex-container-tgc">
                        <div>
                            <label className="title-tgc">Huế - La Vang - Động Phong Nha & Thiên Đường - Bà Nà - Cầu Vàng - Hội An - Đà Nẵng (Khách sạn 4*&5*. Tặng Show Charming. Tour Tiêu Chuẩn)</label>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                <p>Khởi hành : 13/07/2019 06:00</p>
                            </div>
                            <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                <p>Nơi khởi hành :</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                <p>Số chỗ còn nhận : 2</p>
                            </div>
                            <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                <p>Số ngày : 3</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                                <span className="through-line"> 5,390,000đ</span>&nbsp;&nbsp;<span className="price-hight-light"> 6,000,000đ</span>

                            </div>
                        </div>
                        <div className="row">
                            <div className="col-4 col-sm-4 col-md-4 col-lg-4">
                                <p>Số ngày : 3</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-4 col-sm-4 col-md-4 col-lg-4">
                                <button className="form-control font-white btn-countdown"><p><i className="fa fa-clock-o"></i> Còn <Countdown date={Date.now() + 2*24*60*60*1000} /></p></button>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className='row'>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 hidden-xs">
                        <img src="/images/home/tgc1.jpg" alt="" className="img-tgc" />
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 flex-container-tgc">
                        <div>
                            <label className="title-tgc">Huế - La Vang - Động Phong Nha & Thiên Đường - Bà Nà - Cầu Vàng - Hội An - Đà Nẵng (Khách sạn 4*&5*. Tặng Show Charming. Tour Tiêu Chuẩn)</label>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                <p>Khởi hành : 13/07/2019 06:00</p>
                            </div>
                            <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                <p>Nơi khởi hành :</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                <p>Số chỗ còn nhận : 2</p>
                            </div>
                            <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                <p>Số ngày : 3</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                                <span className="through-line"> 5,390,000đ</span>&nbsp;&nbsp;<span className="price-hight-light"> 6,000,000đ</span>

                            </div>
                        </div>
                        <div className="row">
                            <div className="col-4 col-sm-4 col-md-4 col-lg-4">
                                <p>Số ngày : 3</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-4 col-sm-4 col-md-4 col-lg-4">
                            <button className="form-control font-white btn-countdown"><p><i className="fa fa-clock-o"></i> Còn <Countdown date={Date.now() + 1*21*40*60*1000} /></p></button>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 hidden-lg">
                        <img src="/images/home/tgc1.jpg" alt="" className="img-tgc" />
                    </div>
                </div>
                <div className="margin-top">
                    <button className="form-control xem-them-btn">Xem tất cả</button>
                </div>
            </div>
        );
    }
}

export default TourGioChot;
