import React, { Component } from 'react';
import './banner-home.css';

class BannerHome extends Component {
    render() {
        return (
            <div>
                <img src="/images/home/bg-wc.jpg" alt="" className="img-bg" />
            </div>
        );
    }
}

export default BannerHome;