import React, { Component } from 'react';
import './y-kien.css';
import { Slide } from 'react-slideshow-image';

const properties = {
    duration: 5000,
    transitionDuration: 500,
    infinite: true,
    indicators: true,
    arrows: false
}

class YKien extends Component {
    render() {
        return (
            <div className="y-kien option-ykien">
                <Slide {...properties}>
                    <div className="each-slide-ykien bg-ykien">
                        <div className="flex-container-ykien">
                            <div>
                                <h6>CẢM NHẬN CỦA KHÁCH HÀNG TRẦN LỆ THỦY - TOUR CHÂU ÂU NGÀY 27/06/2019</h6>
                                <span><i className="fa fa-calendar"></i> 16/07/19 09:09:38</span><br />
                                <hr/>
                                <span>Cảnh Đà Nẵng và Huế nói chung rất ư là dễ thươngg, êm đềm và thơ mộng, giản dị nhưng rất thư giãn. </span>
                            </div>
                        </div>
                    </div>
                    <div className="each-slide-ykien bg-ykien">
                        <div className="flex-container-ykien">
                            <div>
                                <h6>THƯ CẢM ƠN CỦA KHÁCH HÀNG LÊ MỘNG HẰNG TOUR HUẾ - ĐÀ NẴNG THÁNG 06/2019</h6>
                                <span><i className="fa fa-calendar"></i> 02/07/19 13:25:17</span><br />
                                <hr/>
                                <span>Mình tên Hằng. Hôm nay mình muốn chia sẻ trải ngiệm của chuyến đi tuor ra Đà Nẵng và Huế ngày 26-29/06/2019 vừa qua.
                                        Cảnh Đà Nẵng và Huế nói chung rất ư là dễ thươngg, êm đềm và thơ mộng, giản dị nhưng rất thư giãn. 
                                        Hướng dẫn viên của đoàn là em Lê Ngọc Nhật. Em  tuy tuổi còn trẻ nhưng rất tinh tế, chững</span>
                            </div>
                        </div>
                    </div>
                    <div className="each-slide-ykien bg-ykien">
                        <div className="flex-container-ykien">
                            <div>
                                <h6>THƯ CẢM ƠN VIETRAVEL CỦA KHÁCH HÀNG NGUYỄN AN KHANG TOUR MIỀN BẮC NGÀY 18/06/2019</h6>
                                <span><i className="fa fa-calendar"></i> 28/06/19 14:34:12</span><br />
                                <hr/>
                                <span>Xin chào Vietravel. Là một trong những người vừa tham dự tour du lịch Khám phá Vòng cung Tây Bắc: Hà Nội – Nghĩa Lộ - Tú Lệ - Mù Cang Chải – SAPA – FANSIPAN – Động Tiên Sơn – Lai Châu – Điện Biên – Sơn La – Mộc Châu – Thác Dải Yếm – Mai Châu (Mã tour NDSGN172) của Vietravel, tôi m</span>
                            </div>
                        </div>
                    </div>
                </Slide>
            </div>
        );
    }
}

export default YKien;