import React, { Component } from 'react';
import './home.css';
import TourGioChot from './tour-gio-chot/tour-gio-chot'
import BannerHome from './banner-home/banner-home';
import UuDai from './uu-dai/uu-dai';
import UaThich from './ua-thich/ua-thich';
import ChauLuc from './chauluc/chau-luc';
import BannerReason from './banner-reason/banner-reason';
import YKien from './y-kien/y-kien';

class Home extends Component {
    render() {
        return (
            <div>
                <div className="wrapper-content">
                    <TourGioChot></TourGioChot>
                </div>
                <BannerHome></BannerHome>
                <div className="wrapper-content">
                    <UuDai></UuDai>
                    <UaThich></UaThich>
                    <ChauLuc></ChauLuc>
                </div>
                <div className="title-margin">
                    <h3>VÌ SAO CHỌN TRAVEL.COM.VN</h3>
                </div>
                <BannerReason></BannerReason>
                <div className="title-margin">
                    <h3>Ý KIẾN KHÁCH HÀNG</h3>
                </div>
                <YKien></YKien>
            </div>
        );
    }
}

export default Home;