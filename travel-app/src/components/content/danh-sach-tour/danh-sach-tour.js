import React, { Component } from 'react';
import TinTuc from './tin-tuc/tin-tuc';
import TimKiem from './tim-kiem/tim-kiem';
import './danh-sach-tour.css';
import TourItem from '../tour-item/tour-item';
import Pagination from "react-pagination-library";
import "react-pagination-library/build/css/index.css";
import axios from 'axios';

const data = [{
    "date": "2019/08/09",
    "lst": [
        {
            "date": "09",
            "monthyear": "08/19",
            "image": "/images/trong-nuoc/tour-item/img-4.jpg",
            "title": "Đà Nẵng - Bà Nà - Cầu Vàng - Sơn Trà - KDL Thần Tài - Hội An - Đà Nẵng (Khách sạn 4*. Tặng Show Ký Ức Hội An. Tour Tiêu Chuẩn)",
            "vote": 4.4,
            "total": 2216,
            "id": "NDSGN315-039-090819VJ-D",
            "qty": 4,
            "startday": "2019/08/09",
            "starthour": "5:15",
            "price": "9.690.000",
            "type": "Tiết kiệm"
        }, {
            "date": "09",
            "monthyear": "08/19",
            "image": "/images/trong-nuoc/tour-item/img-5.jpg",
            "title": "Hà Nội - Sapa - Fansipan - Đền Hùng - Tặng vé xe lửa Mường Hoa (Tour Tiết Kiệm)",
            "vote": 4.65,
            "total": 956,
            "id": "NDSGN191-029-090819VJ-V-O",
            "qty": 5,
            "startday": "2019/08/09",
            "starthour": "5:30",
            "price": "6.190.000",
            "type": "Giá tốt"
        }, {
            "date": "09",
            "monthyear": "08/19",
            "image": "/images/trong-nuoc/tour-item/img-6.jpg",
            "title": "Đà Lạt - Đường Hầm Điêu Khắc Đất Sét ( Khách sạn 2 *. Tour Mua Ngay)",
            "vote": 4.5,
            "total": 542,
            "id": "NDSGN5371-038-090819XE-H",
            "qty": 8,
            "startday": "2019/08/09",
            "starthour": "7:30",
            "price": "4.150.000",
            "type": "Tiết kiệm"
        }, {
            "date": "09",
            "monthyear": "08/19",
            "image": "/images/trong-nuoc/tour-item/img-7.jpg",
            "title": "Ninh Chữ - Vườn Nho - Đảo Tôm Hùm Bình Hưng (Resort Tương Đương 4*. Tour Tiết Kiệm)",
            "vote": 4.64,
            "total": 1421,
            "id": "NDSGN560-065-090819XE-V-1",
            "qty": 5,
            "startday": "2019/08/07",
            "starthour": "6:30",
            "price": "2.400.000",
            "type": "Giá tốt"
        }
    ]
}
    , {
    "date": "2019/08/07",
    "lst": [
        {
            "date": "08",
            "monthyear": "08/19",
            "image": "/images/trong-nuoc/tour-item/img-1.jpg",
            "title": "Đà Lạt - Madagui - Đồi chè Cầu Đất - Cà phê Mê Linh (Tour Tiết Kiệm)",
            "vote": 4.6,
            "total": 1421,
            "id": "NDSGN548-057-080819XE-V",
            "qty": 2,
            "startday": "08/08/2019",
            "starthour": "5:30",
            "price": "3.500.000",
            "type": "Tiết kiệm"
        }, {
            "date": "08",
            "monthyear": "08/19",
            "image": "/images/trong-nuoc/tour-item/img-2.jpg",
            "title": "Miền Tây - Làng Bè Châu Đốc - Chùa Hang Phước Điền - Rừng Tràm Trà Sư - Hà Tiên - Rạch Giá - Cần Thơ - Cồn Sơn (Khách sạn 2*&3*, Tour Tiết Kiệm)",
            "vote": 4.49,
            "total": 321,
            "id": "NDSGN849-031-080819XE-V",
            "qty": 5,
            "startday": "2019/08/07",
            "starthour": "5:30",
            "price": "3.290.000",
            "type": "Giá tốt"
        }, {
            "date": "08",
            "monthyear": "08/19",
            "image": "/images/trong-nuoc/tour-item/img-3.jpg",
            "title": "Huế - Động Phong Nha - Bà Nà ( Cầu Vàng) - Hội An - Đà Nẵng (Khách sạn 4*2 đêm.  Tặng Show Charming. Tour Mua Ngay)",
            "vote": 4.38,
            "total": 421,
            "id": "NDSGN3982-029-080819VJ-D-F-1",
            "qty": 2,
            "startday": "2019/08/07",
            "starthour": "8:30",
            "price": "7.500.000",
            "type": "Tiết kiệm"
        }
    ]
}];


class DanhSachTour extends Component {
    constructor() {
        super();
        this.state = { items: [], currentPage: 1 };
    }
    changeCurrentPage = numPage => {
        this.setState({ currentPage: numPage });
    };
    componentDidMount() {
        const testURL = 'http://localhost:3001/products';
        axios.get(testURL,{headers:{    
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }})
            .then(response => {
                console.log(response.data);
                this.setState({ items: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }
    render() {
        return (
            <div>
                <div className="wrapper-content">
                    <div className="row padding-link">
                        <a href="/home">Du lịch </a>&nbsp;»&nbsp;<a href="/trong-nuoc"> Du lịch trong nước</a>
                    </div>
                    <div className="row no-row">
                        <div className="col-12 col-sm-12 col-md-3 col-lg-3">
                            <TimKiem></TimKiem>
                        </div>
                        <div className="col-12 col-sm-12 col-md-9 col-lg-9">
                            <TinTuc></TinTuc>
                            <h3 className="mar-top-title">Danh sách tour du lịch trong nước</h3>
                            {data.map((item) =>
                                item.lst.map((subitem) =>
                                    <div key={subitem.id}>
                                        <TourItem date={subitem.date}
                                            monthyear={subitem.monthyear}
                                            image={subitem.image}
                                            title={subitem.title}
                                            vote={subitem.vote}
                                            total={subitem.total}
                                            id={subitem.id}
                                            qty={subitem.qty}
                                            startday={subitem.startday}
                                            starthour={subitem.starthour}
                                            price={subitem.price}
                                            type={subitem.type}
                                        ></TourItem> <br />
                                    </div>
                                )
                            )}
                            <div className="col-12 col-sm-12 col-md-9 col-lg-9 float-r">
                                <Pagination
                                    currentPage={this.state.currentPage}
                                    totalPages={10}
                                    changeCurrentPage={this.changeCurrentPage}
                                    theme="square-i"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default DanhSachTour;