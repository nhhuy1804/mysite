import React, { Component } from 'react';
import './tin-tuc.css';
import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'

class TinTuc extends Component {
    render() {
        return (
            <div>
                <div className="row">
                    {/* <div className="col-12 col-sm-12 col-md-4 col-lg-4">
                        <img src="/images/trong-nuoc/tin-tuc.png" alt="" className="img-tt" />
                    </div> */}
                    <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                        <div className="col-12 col-sm-12 col-md-12 col-lg-12 mauchudao title-tt unset-left">
                            Du lịch trong nước
                        </div>
                        <div className="col-12 col-sm-12 col-md-12 col-lg-12 unset-left font-italic">
                            Du lịch trong nước luôn là lựa chọn tuyệt vời. Đường bờ biển dài hơn 3260km, những khu bảo tồn thiên nhiên tuyệt vời, những thành phố nhộn nhịp, những di tích lịch sử hào hùng, nền văn hóa độc đáo và hấp dẫn, cùng một danh sách dài những món ăn ngon nhất thế giới, Việt Nam có tất cả những điều đó. Với lịch trình dày, khởi hành đúng thời gian cam kết, Vietravel là công ty lữ hành uy tín nhất hiện nay tại Việt Nam, luôn sẵn sàng phục vụ du khách mọi lúc, mọi nơi, đảm bảo tính chuyên nghiệp và chất lượng dịch vụ tốt nhất thị trường
                        </div>
                        <div className="col-12 col-sm-12 col-md-12 col-lg-12 unset-left">
                            <div className="fb-like"
                                data-href="https://developers.facebook.com/docs/plugins/"
                                data-width=""
                                data-layout="button_count"
                                data-action="like"
                                data-size="small"
                                data-show-faces="true"
                                data-share="true">
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 col-md-12 col-lg-12 row">
                            <Rater total={5} rating={4.5} />
                            <div className="vote-title font-italic"><strong>4.5/5</strong> trong <strong>4.455</strong></div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default TinTuc;