import React, { Component } from 'react';
import Select from 'react-select';
import InputRange from 'react-input-range';
import './tim-kiem.css';
// Import Datepicker
import {DatePickerInput } from 'rc-datepicker';

// Import the default style
import 'rc-datepicker/lib/style.css';
import 'react-input-range/lib/css/index.css';

const options = [
    { value: '0', label: 'Chọn địa điểm' },
    { value: '1', label: 'Hồ Chí Minh' },
    { value: '2', label: 'Hà Nội' },
    { value: '3', label: 'Đà Nẵng' },
    { value: '4', label: 'Cần Thơ' },
    { value: '5', label: 'Nha Trang' },
];

const options2 = [
    { value: '0', label: 'Tất cả' },
    { value: '1', label: 'Dưới 1 triệu' },
    { value: '2', label: 'Từ 1 đến 2 triệu' },
    { value: '3', label: 'Từ 2 đến 4 triệu' },
    { value: '4', label: 'Từ 4 đến 8 triệu' },
    { value: '5', label: 'Trên 8 triệu' },
];

const options3 = [
    { value: '0', label: 'Tour trong nước' },
    { value: '1', label: 'Tour nước ngoài' }
];


class TimKiem extends Component {
    constructor(props, context) {
        super(props, context);

        // Initial state with date
        this.state = {
            // or Date or Moment.js
            selectedDate: '2017-08-13',
            value1: { min: 0, max: 20 },
            value2: { min: 0, max: 7 }
        };

        // This binding is necessary to make `this` work in the callback
        this.onChange = this.onChange.bind(this);
    }

    onChange(date) {
        this.setState({
            selectedDate: date
        });
    }
    render() {
        return (
            <div>
                <div className="wrapper-tk">
                    <div className="title-search">TÌM KIẾM</div>
                    <div className="form-group margin-top-search">
                        <label>Nơi khởi hành</label>
                        <Select placeholder="Nơi khởi hành" options={options} />
                    </div>
                    <div className="form-group margin-top-search">
                        <label>Loại tour</label>
                        <Select placeholder="Loại tour" options={options3} />
                    </div>
                    <div className="form-group margin-top-search">
                        <label>Nơi đến</label>
                        <Select placeholder="Nơi đến" options={options} />
                    </div>
                    <div className="form-group margin-top-search">
                        <label>Ngày khởi hành</label>
                        <DatePickerInput
                            onChange={this.onChange}
                            value={this.state.selectedDate}
                            className='my-custom-datepicker-component'
                        />
                    </div>
                    <div className="form-group margin-top-search">
                        <label>Giá</label>
                        <Select placeholder="Giá" options={options2} />
                    </div>
                    <div className="form-group margin-top-search">
                        <button className="form-control btn-tk">Tìm kiếm</button>
                    </div>
                </div>
                <br />
                <div className="wrapper-tk">
                    <div className="title-search">BỘ LỌC TÌM KIẾM</div>
                    <div className="form-group margin-top-search">
                        <label>Theo giá (triệu VNĐ):</label><br />
                        <div className="margin-range-slide">
                            <InputRange
                                maxValue={20}
                                minValue={0}
                                value={this.state.value1}
                                formatLabel={value => `${value}`}
                                onChange={value1 => this.setState({ value1 })} />
                        </div>
                    </div>
                    <div className="form-group margin-top-search border-bot">
                        <label>Số ngày:</label><br />
                        <div className="margin-range-slide">
                            <InputRange
                                maxValue={7}
                                minValue={0}
                                value={this.state.value2}
                                formatLabel={value => value}
                                onChange={value2 => this.setState({ value2 })} />
                        </div>
                    </div>
                    <div className="form-group margin-top-search border-bot">
                        <label><i className="fa fa-subway"></i> Thông tin vận chuyển:</label><br/>
                        <label><input type="checkbox"/><span>Máy bay</span></label><br/>
                        <label><input type="checkbox"/><span>Ô tô</span></label><br/>
                    </div>
                    <div className="form-group margin-top-search border-bot">
                        <label><i className="fa fa-suitcase"></i> Tình trạng:</label><br/>
                        <label><input type="checkbox"/><span>Còn chỗ</span></label><br/>
                        <label><input type="checkbox"/><span>Hết chỗ</span></label><br/>
                    </div>
                </div>
            </div>
        );
    }
}

export default TimKiem;