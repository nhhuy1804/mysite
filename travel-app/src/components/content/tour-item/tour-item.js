import React, { Component } from 'react';
import './tour-item.css';
import Rater from 'react-rater';

class TourItem extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="row no-row">
                <div className="d-none d-md-block col-md-2 col-lg-2">
                    <div className="form-date">
                        <div className="p-date p-date-st">{this.props.date}</div>
                        <div className="p-date">{this.props.monthyear}</div>
                    </div>
                </div>
                <div className="col-12 col-sm-12 col-md-10 col-lg-10">
                    <div className="border-bot">
                        <label className="title-tour">{this.props.title}</label>
                    </div>
                    <div className="row no-row mar-top">
                        <div className="col-12 col-sm-12 col-md-3 col-lg-3 no-padding">
                            <div className="pos-relative">
                                <div className="badge-promo">
                                    <div className="badge-promo-content">
                                        <div><i className="fa fa-spinner"></i>&nbsp;&nbsp; {this.props.type}</div>
                                    </div>
                                </div>
                                <img src={this.props.image} alt="" className="img-res img-fit" />
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 col-md-9 col-lg-9 no-padding item-container">
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 row no-row">
                                <Rater total={5} rating={this.props.vote} />
                                <div className="vote-title font-italic"><strong>{this.props.vote}/5</strong> trong <strong>{this.props.total}</strong> đánh giá</div>
                            </div>
                            <div className="row no-row font-small">
                                <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                    <i className="fa fa-barcode"></i> {this.props.id}
                                </div>
                                <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                    <i className="fa fa-subway"></i> Số chỗ còn nhận: <strong>{this.props.qty}</strong>
                                </div>
                            </div>
                            <div className="row no-row font-small">
                                <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                    <i className="fa fa-calendar"></i> Ngày đi: <strong>{this.props.startdate}</strong>
                                </div>
                                <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                    <i className="fa fa-clock-o"></i> Giờ đi: <strong>{this.props.starthour}</strong>
                                </div>
                            </div>
                            <div className="row no-row font-small">
                                <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                    <i className="fa fa-money"></i> Giá: <strong className="price-label">{this.props.price}</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default TourItem;