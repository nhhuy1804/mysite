import React, { Component } from 'react';
import './top-header.css';

class TopHeader extends Component {
    // componentDidMount() {
    //     $('.navbar-nav li.dropdown').hover(function() {
    //         debugger
    //         $(this).toggleClass("open");
    //     })
    // }
    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon" />
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul className="navbar-nav">
                            <li className="nav-item dropdown">
                                <a className="nav-link dropdown-toggle" href="/" id="drd-du-lich" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Du lịch
                            </a>
                                <div className="dropdown-menu dropdown-menu-cus" aria-labelledby="drd-du-lich">
                                    <a className="dropdown-item dropdown-item-cus" href="/">Du lịch trong nước</a>
                                    <a className="dropdown-item dropdown-item-cus" href="/">Du lịch nước ngoài</a>
                                    <a className="dropdown-item dropdown-item-cus" href="/">Du lịch tự chọn</a>
                                    <a className="dropdown-item dropdown-item-cus" href="/">Du học</a>
                                </div>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="/">Khách sạn</a>
                            </li>
                            <li className="nav-item dropdown">
                                <a className="nav-link dropdown-toggle" href="/" id="drd-van-chuyen" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Vận chuyển
                            </a>
                                <div className="dropdown-menu dropdown-menu-cus" aria-labelledby="drd-van-chuyen">
                                    <a className="dropdown-item dropdown-item-cus" href="/">Thuê xe</a>
                                    <a className="dropdown-item dropdown-item-cus" href="/">Vé máy bay</a>
                                </div>
                            </li>
                            <li className="nav-item dropdown">
                                <a className="nav-link dropdown-toggle" href="/" id="drd-tin-tuc" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Tin tức
                            </a>
                                <div className="dropdown-menu dropdown-menu-cus" aria-labelledby="drd-tin-tuc">
                                    <a className="dropdown-item dropdown-item-cus" href="/">Tin tức</a>
                                    <a className="dropdown-item dropdown-item-cus" href="/">Cẩm nang du lịch</a>
                                    <a className="dropdown-item dropdown-item-cus" href="/">Kinh nghiệm du lịch</a>
                                </div>
                            </li>
                            <li className="nav-item dropdown">
                                <a className="nav-link dropdown-toggle" href="/" id="drd-khuyen-mai" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Khuyến mại
                                </a>
                                <div className="dropdown-menu dropdown-menu-cus" aria-labelledby="drd-khuyen-mai">
                                    <a className="dropdown-item dropdown-item-cus" href="/">Khuyến mại Hè 2019</a>
                                    <a className="dropdown-item dropdown-item-cus" href="/">Mua tour trả chậm 0%</a>
                                    <a className="dropdown-item dropdown-item-cus" href="/">Khuyến mãi thẻ JCB</a>
                                </div>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="/">VietTravelPlus</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="/">Liên hệ</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        );
    }
}

export default TopHeader;
