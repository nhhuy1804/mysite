import React, { Component } from 'react';
import './bot-footer.css';

class BotFooter extends Component {
    render() {
        return (
            <div className="wrapper-header flex-container-bot">
                <div className="center">
                    <p>Bản quyền của Vietravel ® 2016. Bảo lưu mọi quyền. Ghi rõ nguồn "www.travel.com.vn" ® khi sử dụng lại thông tin từ website này</p>
                    <p>Số GPKD: 0300465937 do Sở Kế Hoạch và Đầu Tư Thành Phố Hồ Chí Minh cấp ngày 27/09/2010</p>
                    <p>Giấy phép số: 239/GP-TTĐT do Cục Quản Lý Phát Thanh, Truyền Hình và Thông Tin Điện Tử cấp ngày 31/12/2008</p>
                </div>
            </div>
        );
    }
}

export default BotFooter;
