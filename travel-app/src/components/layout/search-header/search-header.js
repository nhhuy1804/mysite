import React, { Component } from 'react';
import './search-header.css';
import Select from 'react-select';

// Import Datepicker
import { DatePicker, DatePickerInput } from 'rc-datepicker';

// Import the default style
import 'rc-datepicker/lib/style.css';

const options = [
    { value: '0', label: 'Chọn địa điểm' },
    { value: '1', label: 'Hồ Chí Minh' },
    { value: '2', label: 'Hà Nội' },
    { value: '3', label: 'Đà Nẵng' },
    { value: '4', label: 'Cần Thơ' },
    { value: '5', label: 'Nha Trang' },
];

const options1 = [
    { value: '0', label: 'Tất cả' },
    { value: '1', label: 'Cao cấp' },
    { value: '2', label: 'Tiêu chuẩn' },
    { value: '3', label: 'Tiết kiệm' },
    { value: '4', label: 'Giá tốt' },
];

const options2 = [
    { value: '0', label: 'Tất cả' },
    { value: '1', label: 'Dưới 1 triệu' },
    { value: '2', label: 'Từ 1 đến 2 triệu' },
    { value: '3', label: 'Từ 2 đến 4 triệu' },
    { value: '4', label: 'Từ 4 đến 8 triệu' },
    { value: '5', label: 'Trên 8 triệu' },
];

class SearchHeader extends Component {
    constructor(props, context) {
        super(props, context);

        // Initial state with date
        this.state = {
            // or Date or Moment.js
            selectedDate: '2017-08-13'
        };

        // This binding is necessary to make `this` work in the callback
        this.onChange = this.onChange.bind(this);
    }

    onChange(date) {
        this.setState({
            selectedDate: date
        });
    }
    render() {
        return (
            <div>
                <div className="flex-container-search">
                    <div className="padding-search box-trong-nuoc" data-toggle="collapse" data-target="#search-trong-nuoc">
                        <img src="/images/search/st-trong-nuoc.png" alt="" className="img-responsive-search margin-bot-img" />
                        <p className="font-search">TÌM TOUR</p>
                        <p className="font-search">------------</p>
                        <p className="font-search hight-light-text-1">TRONG NƯỚC</p>
                    </div>
                    <div className="dropdown-menu search-bar" id="search-trong-nuoc">
                        <div className="row no-margin">
                            <div className="col-12 col-sm-4 col-md-4 col-lg-2 margin-bot">
                                <label className="hight-light-text-1">NƠI KHỞI HÀNH</label>
                                <Select className="drd-search"
                                    placeholder="Nơi khởi hành"
                                    options={options}
                                />
                            </div>
                            <div className="col-12 col-sm-4 col-md-4 col-lg-2 margin-bot">
                                <label className="hight-light-text-1">NƠI ĐẾN</label>
                                <Select className="drd-search" placeholder="Nơi đến" options={options} />
                            </div>
                            <div className="col-12 col-sm-4 col-md-4 col-lg-2 margin-bot">
                                <label className="hight-light-text-1">NGÀY KHỞI HÀNH</label>
                                <div className="date-picker">
                                    <DatePickerInput
                                        onChange={this.onChange}
                                        value={this.state.selectedDate}
                                        className='my-custom-datepicker-component'
                                    />
                                </div>
                            </div>
                            <div className="col-12 col-sm-4 col-md-4 col-lg-2 margin-bot">
                                <label className="hight-light-text-1">DÒNG TOUR</label>
                                <Select className="drd-search" options={options1} placeholder="Tất cả" />
                            </div>
                            <div className="col-12 col-sm-4 col-md-4 col-lg-2 margin-bot">
                                <label className="hight-light-text-1">GIÁ</label>
                                <Select className="drd-search" options={options2} placeholder="Tất cả" />
                            </div>
                            <div className="col-12 col-sm-4 col-md-4 col-lg-2 margin-bot">
                                <button className="form-control search-btn search-btn-1"><i className="fa fa-search"></i> Tìm kiếm</button>
                            </div>
                        </div>
                    </div>

                    <div className="padding-search box-nuoc-ngoai" data-toggle="collapse" data-target="#search-nuoc-ngoai">
                        <img src="/images/search/st-nuoc-ngoai.png" alt="" className="img-responsive-search margin-bot-img" />
                        <p className="font-search">TÌM TOUR</p>
                        <p className="font-search">------------</p>
                        <p className="font-search hight-light-text-2">NƯỚC NGOÀI</p>
                    </div>
                    <div className="dropdown-menu search-bar" id="search-nuoc-ngoai">
                        <div className="row no-margin">
                            <div className="col-12 col-sm-4 col-md-4 col-lg-2 margin-bot">
                                <label className="hight-light-text-2">NƠI KHỞI HÀNH</label>
                                <Select className="drd-search"
                                    placeholder="Nơi khởi hành"
                                    options={options}
                                />
                            </div>
                            <div className="col-12 col-sm-4 col-md-4 col-lg-2 margin-bot">
                                <label className="hight-light-text-2">NƠI ĐẾN</label>
                                <Select className="drd-search" placeholder="Nơi đến" options={options} />
                            </div>
                            <div className="col-12 col-sm-4 col-md-4 col-lg-2 margin-bot">
                                <label className="hight-light-text-2">NGÀY KHỞI HÀNH</label>
                                <div className="date-picker">
                                    <DatePickerInput
                                        onChange={this.onChange}
                                        value={this.state.selectedDate}
                                        className='my-custom-datepicker-component'
                                    />
                                </div>
                            </div>
                            <div className="col-12 col-sm-4 col-md-4 col-lg-2 margin-bot">
                                <label className="hight-light-text-2">DÒNG TOUR</label>
                                <Select className="drd-search" options={options1} placeholder="Tất cả" />
                            </div>
                            <div className="col-12 col-sm-4 col-md-4 col-lg-2 margin-bot">
                                <label className="hight-light-text-2">GIÁ</label>
                                <Select className="drd-search" options={options2} placeholder="Tất cả" />
                            </div>
                            <div className="col-12 col-sm-4 col-md-4 col-lg-2 margin-bot">
                                <button className="form-control search-btn search-btn-2"><i className="fa fa-search"></i> Tìm kiếm</button>
                            </div>
                        </div>
                    </div>

                    <div className="padding-search box-tu-chon">
                        <img src="/images/search/st-tu-chon.png" alt="" className="img-responsive-search margin-bot-img" />
                        <p className="font-search">DU LỊCH</p>
                        <p className="font-search">------------</p>
                        <p className="font-search hight-light-text-3">TỰ CHỌN</p>
                    </div>
                    <div className="padding-search box-booking">
                        <img src="/images/search/st-booking.png" alt="" className="img-responsive-search margin-bot-img" />
                        <p className="font-search">TÌM KIẾM</p>
                        <p className="font-search">------------</p>
                        <p className="font-search hight-light-text-4">BOOKING</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default SearchHeader;
