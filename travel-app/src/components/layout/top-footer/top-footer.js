import React, { Component } from 'react';
import './top-footer.css';

const slideImages = [
    '/images/footer/123pay.png',
    '/images/footer/jcb.png',
    '/images/footer/master.png',
    '/images/footer/other1.png',
    '/images/footer/other2.png',
    '/images/footer/other3.png',
    '/images/footer/visa.png',
    '/images/footer/vnpay.jpg'
];

class TopFooter extends Component {
    render() {
        return (
            <div className="wrapper-header wrapper-footer row">
                <div className="col-12 col-sm-12 col-md-3 col-lg-3">
                    <strong className="mauchudao">LIÊN HỆ</strong>
                    <p className="footer-text-margin">190 Pasteur, Phường 6, Quận 3, Tp. HCM</p>
                    <p className="footer-text-margin"><strong>Điện thoại:</strong> (+84 28) 3822 8898</p>
                    <p className="footer-text-margin"><strong>Fax:</strong> (+84 28) 3829 9142</p>
                    <p className="footer-text-margin"><strong>Email:</strong> info@vietravel.com</p>
                    <p className="footer-text-margin"><strong>Hotline:</strong> 1900 1839 (08h-23h)</p>
                    <div className="line-dot"></div>
                    <p className="footer-text-margin"><i className="fa fa-circle mauchudao margin-right-1vw"></i>  Vietravel toolbar</p>
                    <p className="footer-text-margin"><i className="fa fa-circle mauchudao margin-right-1vw"></i>  Chính sách riêng tư</p>
                    <p className="footer-text-margin"><i className="fa fa-circle mauchudao margin-right-1vw"></i>  Thỏa thuận sử dụng</p>
                    <br />
                    <strong className="mauchudao">CHẤP NHẬN THANH TOÁN</strong>
                    <div className="flex-container-pay">
                        <div className="no-margin-left">
                            <img src="/images/footer/123pay.png" alt="" className="img-top-footer" />
                        </div>
                        <div>
                            <img src="/images/footer/jcb.png" alt="" className="img-top-footer" />
                        </div>
                        <div>
                            <img src="/images/footer/master.png" alt="" className="img-top-footer" />
                        </div>
                        <div>
                            <img src="/images/footer/other1.png" alt="" className="img-top-footer" />
                        </div>
                    </div>
                    <div className="flex-container-pay">
                        <div className="no-margin-left">
                            <img src="/images/footer/other2.png" alt="" className="img-top-footer" />
                        </div>
                        <div>
                            <img src="/images/footer/other3.png" alt="" className="img-top-footer" />
                        </div>
                        <div>
                            <img src="/images/footer/visa.png" alt="" className="img-top-footer" />
                        </div>
                        <div>
                            <img src="/images/footer/vnpay.jpg" alt="" className="img-top-footer" />
                        </div>
                    </div>
                    <br />
                    <strong className="mauchudao">ĐƯỢC ĐIỀU HÀNH BỞI</strong>
                    <div className="col-10 col-sm-10 col-md-6 col-lg-6">
                        <img src="/images/footer/logo_Vietravel.png" alt="" className="img-top-footer-logo" />
                    </div>
                </div>
                <div className="col-12 col-sm-12 col-md-3 col-lg-3">
                    <strong className="mauchudao">KẾT NỐI VỚI CHÚNG TÔI</strong>
                    <div className="flex-container-pay">
                        <div className="no-margin-left">
                            <img src="/images/footer/fb.png" alt="" className="img-top-footer-si bot-si-margin" />
                        </div>
                        <div>
                            <img src="/images/footer/instagram.png" alt="" className="img-top-footer-si" />
                        </div>
                        <div>
                            <img src="/images/footer/tw.png" alt="" className="img-top-footer-si" />
                        </div>
                        <div>
                            <img src="/images/footer/yt.png" alt="" className="img-top-footer-si" />
                        </div>
                    </div>
                    <strong className="mauchudao">NEWSLETTER</strong>
                    <div>
                        <input type="text" placeholder="Email của bạn" name="search" className="input-text" />
                        <button type="submit" className="input-button footer-btn-border"><i className="fa fa-paper-plane"></i></button>
                    </div>
                    <br />
                    <br />
                    <strong className="mauchudao">TÌM KIẾM</strong>
                    <div>
                        <input type="text" placeholder="Tour Du lịch, Điểm đến..." name="search" className="input-text" />
                        <button type="submit" className="input-button footer-btn-border"><i className="fa fa-search"></i></button>
                    </div>
                    <br />
                    <br />
                    <strong className="mauchudao">ỨNG DỤNG DI ĐỘNG</strong>
                    <div className="flex-container-pay-code">
                        <div className="no-margin-left">
                            <img src="/images/footer/a.png" alt="" className="" />
                            <p className="text-format">Android</p>
                        </div>
                        <div>
                            <img src="/images/footer/i.png" alt="" className="" />
                            <p className="text-format">iOS</p>
                        </div>
                    </div>
                </div>
                <div className="col-12 col-sm-12 col-md-6 col-lg-6 display-row">
                    <div className="col-6 col-sm-4 col-md-4 col-lg-4">
                        <strong className="mauchudao">TRONG NƯỚC</strong>
                        <p className="footer-text-margin">Hà Nội</p>
                        <p className="footer-text-margin">Hạ Long</p>
                        <p className="footer-text-margin">Ninh Bình</p>
                        <p className="footer-text-margin">Quảng Bình</p>
                        <p className="footer-text-margin">Lào Cai</p>
                        <p className="footer-text-margin">Đà Nẵng</p>
                        <p className="footer-text-margin">Qui Nhơn</p>
                        <p className="footer-text-margin">Nha Trang</p>
                        <p className="footer-text-margin">Phan Thiết</p>
                        <p className="footer-text-margin">Đà Lạt</p>
                        <p className="footer-text-margin">Vũng Tàu</p>
                        <p className="footer-text-margin">Cần Thơ</p>
                        <p className="footer-text-margin">Phú Quốc</p>
                        <p className="footer-text-margin">Cà Mau</p>
                        <p className="footer-text-margin">Kiên Giang</p>
                    </div>
                    <div className="col-6 col-sm-4 col-md-4 col-lg-4">
                        <strong className="mauchudao">NƯỚC NGOÀI</strong>
                        <p className="footer-text-margin">Cambodia</p>
                        <p className="footer-text-margin">Singapore</p>
                        <p className="footer-text-margin">Thái Lan</p>
                        <p className="footer-text-margin">Nhật Bản</p>
                        <p className="footer-text-margin">Hàn Quốc</p>
                        <p className="footer-text-margin">Trung Quốc</p>
                        <p className="footer-text-margin">Hongkong-Macau</p>
                        <p className="footer-text-margin">Pháp</p>
                        <p className="footer-text-margin">Nga</p>
                        <p className="footer-text-margin">Mỹ</p>
                        <p className="footer-text-margin">Brazil</p>
                        <p className="footer-text-margin">Úc</p>
                        <p className="footer-text-margin">New Zealand</p>
                        <p className="footer-text-margin">Nam Phi</p>
                        <p className="footer-text-margin">Kenya</p>
                    </div>
                    <div className="col-6 col-sm-4 col-md-4 col-lg-4">
                        <strong className="mauchudao">DÒNG TOUR</strong>
                        <p className="footer-text-margin">Cao cấp</p>
                        <p className="footer-text-margin">Tiêu chuẩn</p>
                        <p className="footer-text-margin">Tiết kiệm</p>
                        <p className="footer-text-margin">Rất tốt</p>
                        <br/>
                        <strong className="mauchudao">THÔNG TIN</strong>
                        <p className="footer-text-margin">Tin tức</p>
                        <p className="footer-text-margin">Tạp chí du lịch</p>
                        <p className="footer-text-margin">Cẩm nang du lịch</p>
                        <p className="footer-text-margin">Kinh nghiệm du lịch</p>
                        <p className="footer-text-margin">Sitemap</p>
                        <p className="footer-text-margin">Liên hệ</p>
                        <p className="footer-text-margin">FAQ</p>
                        <br/>
                        <strong className="mauchudao">CHỨNG NHẬN</strong>
                        <div className="no-margin-left">
                            <img src="/images/footer/chungnhan.png" alt="" className="" />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default TopFooter;
