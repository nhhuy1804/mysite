import React, { Component } from 'react';
import './option-header.css';
import { Slide } from 'react-slideshow-image';

const properties = {
    duration: 5000,
    transitionDuration: 500,
    infinite: true,
    indicators: true,
    arrows: true,
    onChange: (oldIndex, newIndex) => {
    }
}

class OptionHeader extends Component {
    render() {
        return (
            <div className="option">
                <Slide {...properties}>
                    <div className="each-slide bg-color-white">
                        <div className="flex-container-op">
                            <div>
                                <img src="/images/option/hoichoitc.png" alt="" className="img-option" />
                                <p className="font-option">
                                    Tour Di sản miền Trung
                                </p>
                            </div>
                            <div>
                                <img src="/images/option/moi.png" alt="" className="img-option" />
                                <p className="font-option">
                                    Tour mới
                                </p>
                            </div>
                            <div>
                                <img src="/images/option/buddha.png" alt="" className="img-option" />
                                <p className="font-option">
                                    Tour hành hương
                                </p>
                            </div>
                            <div>
                                <img src="/images/option/thailand.png" alt="" className="img-option" />
                                <p className="font-option">
                                    Khuyễn mại Tour Thái Lan
                                </p>
                            </div>
                            <div>
                                <img src="/images/option/snow.png" alt="" className="img-option" />
                                <p className="font-option">
                                    Tour châu Âu
                                </p>
                            </div>
                            <div>
                                <img src="/images/option/bestdeals.png" alt="" className="img-option" />
                                <p className="font-option">
                                    Ưu đãi khủng
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="each-slide bg-color-white">
                        <div className="flex-container-op">
                            <div>
                                <img src="/images/option/nuthan.png" alt="" className="img-option" />
                                <p className="font-option">
                                    Tour Hoa Kì
                                </p>
                            </div>
                            <div>
                                <img src="/images/option/moi.png" alt="" className="img-option" />
                                <p className="font-option">
                                    Tour mới
                                </p>
                            </div>
                            <div>
                                <img src="/images/option/Okinawa.png" alt="" className="img-option" />
                                <p className="font-option">
                                Charter Ibaraki
                                </p>
                            </div>
                            <div>
                                <img src="/images/option/family.png" alt="" className="img-option" />
                                <p className="font-option">
                                    Ưu đãi Ngày Gia Đình
                                </p>
                            </div>
                            <div>
                                <img src="/images/option/2-9.png" alt="" className="img-option" />
                                <p className="font-option">
                                    Tour lễ 2/9
                                </p>
                            </div>
                            <div>
                                <img src="/images/option/pupil.png" alt="" className="img-option" />
                                <p className="font-option">
                                    Du học
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="each-slide bg-color-white">
                    <div className="flex-container-op">
                            <div>
                                <img src="/images/option/premium.png" alt="" className="img-option" />
                                <p className="font-option">
                                    Cao Cấp
                                </p>
                            </div>
                            <div>
                                <img src="/images/option/deluxe.png" alt="" className="img-option" />
                                <p className="font-option">
                                    Tiêu Chuẩn
                                </p>
                            </div>
                            <div>
                                <img src="/images/option/tietkiem.png" alt="" className="img-option" />
                                <p className="font-option">
                                Tiết Kiệm
                                </p>
                            </div>
                            <div>
                                <img src="/images/option/giasoc.png" alt="" className="img-option" />
                                <p className="font-option">
                                    Giá Tốt
                                </p>
                            </div>
                            <div>
                                <img src="/images/option/batdongsan.png" alt="" className="img-option" />
                                <p className="font-option">
                                    Tour Bất Động Sản
                                </p>
                            </div>
                            <div>
                                <img src="/images/option/duthuyen.png" alt="" className="img-option" />
                                <p className="font-option">
                                    Tour Du thuyền
                                </p>
                            </div>
                        </div>
                    </div>
                </Slide>
            </div>
        );
    }
}

export default OptionHeader;
