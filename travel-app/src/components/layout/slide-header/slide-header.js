import React, { Component } from 'react';
import './slide-header.css';
import { Slide } from 'react-slideshow-image';

const slideImages = [
    '/images/slide/slide1.jpg',
    '/images/slide/slide2.jpg',
    '/images/slide/slide3.jpg',
    '/images/slide/slide4.jpg',
    '/images/slide/slide5.jpg',
    '/images/slide/slide6.jpg'
];

const properties = {
    duration: 5000,
    transitionDuration: 500,
    infinite: true,
    indicators: true,
    arrows: true
}

class SlideHeader extends Component {
    render() {
        return (
            <div>
                <Slide {...properties} >
                    <div className="each-slide" >
                        <div style={{ 'backgroundImage': `url(${slideImages[0]})` }} >
                        </div>
                    </div>
                    <div className="each-slide">
                        <div style={{ 'backgroundImage': `url(${slideImages[1]})` }}>
                        </div>
                    </div>
                    <div className="each-slide" >
                        <div style={{ 'backgroundImage': `url(${slideImages[2]})` }} >
                        </div>
                    </div>
                    <div className="each-slide" >
                        <div style={{ 'backgroundImage': `url(${slideImages[3]})` }} >
                        </div>
                    </div>
                    <div className="each-slide" >
                        <div style={{ 'backgroundImage': `url(${slideImages[4]})` }} >
                        </div>
                    </div>
                    <div className="each-slide" >
                        <div style={{ 'backgroundImage': `url(${slideImages[5]})` }} >
                        </div>
                    </div>
                </Slide>
            </div>
        );
    }
}

export default SlideHeader;