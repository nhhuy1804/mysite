import React, { Component } from 'react';
import './center-header.css';

class CenterHeader extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-12 col-sm-12 col-md-4 col-lg-4">
                    <img src="/images/logo.png" alt="" className="img-responsive" />
                </div>
                <div className="col-12 col-sm-12 col-md-8 col-lg-8">
                    <div className="row col-12 col-sm-12 col-md-8 col-lg-6 fl-right max-height">
                        <div className="col-3 col-sm-3 col-md-3 col-lg-3 flex-container no-padding">
                            <div>
                                <img src="/images/user.png" alt="" className="img-user dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" />
                                <div className="dropdown-menu">
                                    <a className="dropdown-item dropdown-item-usr dot-line" href="/"><i className="fa fa-sign-in icon-login-regist"></i> Đăng nhập</a>
                                    <a className="dropdown-item dropdown-item-usr" href="/"><i className="fa fa-registered icon-login-regist"></i> Đăng kí</a>
                                </div>
                            </div>
                        </div>
                        <div className="col-4 col-sm-4 col-md-4 col-lg-4 flex-container-curr no-padding">
                            <div>
                                <img src="/images/currency/usa.png" alt="" className="img-user" role="button" />
                            </div>
                            <div>
                                <span className="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >VIE</span>
                                <div className="dropdown-menu dropdown-menu-curr">
                                    <a className="dropdown-item dropdown-item-usr dot-line" href="/"><strong className="mauchudao">USD</strong> - US DOLLAR</a>
                                    <a className="dropdown-item dropdown-item-usr dot-line" href="/"><strong className="mauchudao">BAHT</strong> - THAI BAHT</a>
                                    <a className="dropdown-item dropdown-item-usr dot-line" href="/"><strong className="mauchudao">AUD</strong> - AUST.DOLLAR CANADIAN</a>
                                    <a className="dropdown-item dropdown-item-usr dot-line" href="/"><strong className="mauchudao">CAD</strong> - CANADIAN DOLLAR</a>
                                    <a className="dropdown-item dropdown-item-usr dot-line" href="/"><strong className="mauchudao">EUR</strong> - EURO</a>
                                    <a className="dropdown-item dropdown-item-usr dot-line" href="/"><strong className="mauchudao">SGD</strong> - SINGAPORE DOLLAR</a>
                                    <a className="dropdown-item dropdown-item-usr dot-line" href="/"><strong className="mauchudao">HKD</strong> - HONGKONG DOLLAR</a>
                                    <a className="dropdown-item dropdown-item-usr dot-line" href="/"><strong className="mauchudao">VND</strong> - VIETNAMESE DONG</a>
                                    <a className="dropdown-item dropdown-item-usr dot-line" href="/"><strong className="mauchudao">JPY</strong> - JAPANESE YEN</a>
                                    <a className="dropdown-item dropdown-item-usr dot-line" href="/"><strong className="mauchudao">GBP</strong> - BRITISH POUND</a>
                                    <a className="dropdown-item dropdown-item-usr dot-line" href="/"><strong className="mauchudao">NZD</strong> - NEW ZEALAND DOLLAR</a>
                                    <a className="dropdown-item dropdown-item-usr dot-line" href="/"><strong className="mauchudao">CHF</strong> - SWISS FRANCE</a>
                                    <a className="dropdown-item dropdown-item-usr dot-line" href="/"><strong className="mauchudao">RUB</strong> - RUSSIAN RUBLE</a>
                                    <a className="dropdown-item dropdown-item-usr dot-line" href="/"><strong className="mauchudao">DKK</strong> - DANISH KRONE</a>
                                    <a className="dropdown-item dropdown-item-usr" href="/"><strong className="mauchudao">KRW</strong> - SOUTH KOREAN WON</a>
                                </div>
                            </div>
                        </div>
                        <div className="col-5 col-sm-5 col-md-5 col-lg-5 flex-container-con no-padding">
                            <div>
                                <label className="mauchudao num-contact mar-top">1900 1839</label><br />
                                <label className="font-sm mar-bot">cước 1000đ/phút</label>
                            </div>
                            <div>
                                <span className="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ></span>
                                <div className="dropdown-menu dropdown-menu-con">
                                    <a className="dropdown-item" href="/"><strong className="mauchudao">HỒ CHÍ MINH</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Tổng đài:</label> <strong className=" float-r">(028)3822.8898</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Du lịch trong nước:</label> <strong className="float-r">0938.301.399</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Du lịch nước ngoài:</label> <strong className="float-r">0938.301.388</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Dịch vụ khách hàng:</label> <strong className="float-r">0938.301.234</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">TT Điều hành HDV:</label> <strong className="float-r">0938.908.690</strong></a>
                                    <a className="dropdown-item" href="/"><strong className="mauchudao">HÀ NỘI</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Du lịch trong nước:</label> <strong className="float-r">0989.370.033</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Du lịch nước ngoài:</label> <strong className="float-r">0983.160.022</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Dịch vụ khách hàng:</label> <strong className="float-r">0938.301.234</strong></a>
                                    <a className="dropdown-item" href="/"><strong className="mauchudao">ĐÀ NẴNG</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Du lịch trong nước:</label> <strong className="float-r">0905.173.331</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Du lịch nước ngoài:</label> <strong className="float-r">0932.472.310</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Dịch vụ khách hàng:</label> <strong className="float-r">0938.301.234</strong></a>
                                    <a className="dropdown-item" href="/"><strong className="mauchudao">CẦN THƠ</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Du lịch trong nước:</label> <strong className="float-r">0907.213.122</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Du lịch nước ngoài:</label> <strong className="float-r">0903.114.664</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Dịch vụ khách hàng:</label> <strong className="float-r">0938.301.234</strong></a>
                                    <a className="dropdown-item" href="/"><strong className="mauchudao">HẢI PHÒNG</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Du lịch trong nước:</label> <strong className="float-r">0936.900.095</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Du lịch nước ngoài:</label> <strong className="float-r">0936.900.085</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Dịch vụ khách hàng:</label> <strong className="float-r">0938.301.234</strong></a>
                                    <a className="dropdown-item" href="/"><strong className="mauchudao">NHA TRANG</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Du lịch trong nước:</label> <strong className="float-r">‎‎093.1616.555</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Du lịch nước ngoài:</label> <strong className="float-r">0988.608.286</strong></a>
                                    <a className="dropdown-item" href="/"><label className="mauchudao float-l">Dịch vụ khách hàng:</label> <strong className="float-r">0938.301.234</strong></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CenterHeader;
